﻿using mvc_project.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace mvc_project.Services
{
    public class CountriesService
    {
        readonly HttpClient httpClient;

        public CountriesService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Page<Country>> GetCountries()
        {
            using HttpResponseMessage responseMessage = await httpClient.GetAsync("countries/all");
            string responseContent = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Page<Country>>(responseContent);
        }

        public async Task<Country> CreateCountry(Country countryToCreate)
        {
            string content = JsonConvert.SerializeObject(countryToCreate);
            using HttpContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
            using HttpResponseMessage responseMessage = await httpClient.PutAsync("countries/add", httpContent);
            string responseContent = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Country>(responseContent);
        }

        public async Task DeleteCountry(short countryId)
        {
            using HttpResponseMessage responseMessage = await httpClient.DeleteAsync($"countries/remove?id={countryId}");
        }
    }
}
