﻿using mvc_project.Models;
using Newtonsoft.Json;
using System.Text;

namespace mvc_project.Services
{
    public class CitiesService
    {
        HttpClient httpClient;

        public CitiesService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Page<City>> GetCititesByCountry(short countryId)
        {
            using HttpResponseMessage responseMessage = await httpClient.GetAsync($"cities/all?countryId={countryId}");
            string responseContent = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Page<City>>(responseContent);
        }

        public async Task<City> GetCityByName(string cityName)
        {
            using HttpResponseMessage responseMessage = await httpClient.GetAsync($"cities/{cityName}");
            string responseContent = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<City>(responseContent);
        }

        public async Task<City> CreateCity(City cityToCreate)
        {
            string content = JsonConvert.SerializeObject(cityToCreate);
            using HttpContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
            using HttpResponseMessage responseMessage = await httpClient.PutAsync("cities/add", httpContent);
            string responseContent = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<City>(responseContent);
        }
    }
}
