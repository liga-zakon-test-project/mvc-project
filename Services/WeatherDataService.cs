﻿using mvc_project.Models;
using Newtonsoft.Json;

namespace mvc_project.Services
{
    public class WeatherDataService
    {
        readonly HttpClient httpClient;

        public WeatherDataService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Page<WeatherData>> GetWeatherDataByCity(short cityId, int pageNum = 0, int itemsPerPage = 100)
        {
            using HttpResponseMessage responseMessage = await httpClient.GetAsync($"weatherData/all?cityId={cityId}&pageNumber={pageNum}&itemsPerPage={itemsPerPage}");
            string responseContent = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Page<WeatherData>>(responseContent);
        }

        public async Task<Page<WeatherData>> GetWeatherDataByCityAndBetweenDates(short cityId, int pageNum = 0, int itemsPerPage = 100, long date1 = 0, long date2 = 0)
        {
            using HttpResponseMessage responseMessage = await httpClient.GetAsync($"weatherData/betweenDates?cityId={cityId}&pageNumber={pageNum}&itemsPerPage={itemsPerPage}&date1={date1}&date2={date2}");
            string responseContent = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Page<WeatherData>>(responseContent);
        }
    }
}
