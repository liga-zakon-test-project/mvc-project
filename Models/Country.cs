﻿using Newtonsoft.Json;

namespace mvc_project.Models
{
    public class Country
    {
        [JsonProperty("id")]
        public required short Id { get; set; }
        [JsonProperty("name")]
        public required string Name { get; set; }
    }
}
