﻿using Newtonsoft.Json;

namespace mvc_project.Models
{
    public class WeatherData
    {
        [JsonProperty("temperature")]
        public required float Temperature { get; set; }

        [JsonProperty("pressure")]
        public required short Pressure { get; set; }

        [JsonProperty("humidity")]
        public required short Humidity { get; set; }

        [JsonProperty("timeOfRecording")]
        public required DateTime TimeOfRecording { get; set; }

        [JsonProperty("city")]
        public required City City { get; set; }
    }
}
