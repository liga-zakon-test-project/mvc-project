﻿using Newtonsoft.Json;

namespace mvc_project.Models
{
    public class City
    {
        [JsonProperty("id")]
        public required long Id { get; set; }
        [JsonProperty("name")]
        public required string Name { get; set; }
        [JsonProperty("latitude")]
        public required float Latitude { get; set; }
        [JsonProperty("longitude")]
        public required float Longitude { get; set; }
        [JsonProperty("country")]
        public required Country Country { get; set; }
    }
}
