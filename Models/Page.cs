﻿using Newtonsoft.Json;

namespace mvc_project.Models
{
    public class Page<T>
    {
        [JsonProperty(PropertyName = "content")]
        public required List<T> PageContent { get; set; }

        [JsonProperty(PropertyName = "totalPages")]
        public required int TotalPages { get; set; }

        [JsonProperty(PropertyName = "numberOfElements")]
        public required int NumberOfElements { get; set; }
    }
}
