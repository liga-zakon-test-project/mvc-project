﻿using Microsoft.AspNetCore.Mvc;
using mvc_project.Models;
using mvc_project.Services;
using System.Net;

namespace mvc_project.Controllers
{
    public class CountriesController : Controller
    {
        readonly CountriesService countryService;

        public CountriesController(CountriesService countryService)
        {
            this.countryService = countryService;
        }

        public async Task<IActionResult> All()
        {
            Page<Country> result = await countryService.GetCountries();
            return View(result);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Country country)
        {
            await countryService.CreateCountry(country);
            return RedirectToAction("All", "Countries");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(short countryId, string countryName)
        {
            Country country = new Country { Id = countryId, Name = countryName };
            return View(country);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> Delete(short countryId)
        {
            await countryService.DeleteCountry(countryId);
            return RedirectToAction("All", "Countries");
        }
    }
}
