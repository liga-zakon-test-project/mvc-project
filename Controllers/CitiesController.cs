﻿using Microsoft.AspNetCore.Mvc;
using mvc_project.Models;
using mvc_project.Services;

namespace mvc_project.Controllers
{
    public class CitiesController : Controller
    {
        readonly CitiesService citiesService;

        public CitiesController(CitiesService citiesService)
        {
            this.citiesService = citiesService;
        }

        public async Task<IActionResult> All(short countryId)
        {
            Page<City> result = await citiesService.GetCititesByCountry(countryId);
            return View(result);
        }

        public async Task<IActionResult> Search(string city)
        {
            City result = await citiesService.GetCityByName(city);
            return View(result);
        }

        [HttpGet]
        public async Task<IActionResult> Create(short countryId)
        {
            City city = new City { Country = new Country { Id = countryId, Name = "" }, Id = 0, Name = "", Latitude = 0, Longitude = 0 };
            return View(city);
        }

        [HttpPost]
        public async Task<IActionResult> Create(short countryId, City city)
        {
            city.Country = new Country { Id = countryId, Name = "" };
            await citiesService.CreateCity(city);
            return RedirectToAction("All", "Cities", new { countryId = city.Country.Id });
        }
    }
}
