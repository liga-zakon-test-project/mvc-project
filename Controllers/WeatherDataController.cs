﻿using Microsoft.AspNetCore.Mvc;
using mvc_project.Models;
using mvc_project.Services;

namespace mvc_project.Controllers
{
    public class WeatherDataController : Controller
    {
        readonly WeatherDataService weatherDataService;

        public WeatherDataController(WeatherDataService weatherDataService)
        {
            this.weatherDataService = weatherDataService;
        }

        public async Task<IActionResult> All(short city)
        {
            Page<WeatherData> result = await weatherDataService.GetWeatherDataByCity(city, 0, 100);
            return View(result);
        }

        public async Task<IActionResult> BetweenDates(short city, long date1 = 0, long date2 = 0)
        {
            Page<WeatherData> result = await weatherDataService.GetWeatherDataByCityAndBetweenDates(city, 0, 100, date1, date2);
            return View(result);
        }
    }
}
