using mvc_project.Services;

var builder = WebApplication.CreateBuilder(args);

HttpClient httpClient = new HttpClient();
httpClient.BaseAddress = new Uri("http://localhost:8080");

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSingleton(typeof(CountriesService));
builder.Services.AddSingleton(typeof(CitiesService));
builder.Services.AddSingleton(typeof(WeatherDataService));
builder.Services.AddSingleton(httpClient);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapControllerRoute(name: "Countries", pattern: "{controller=Countries}/{action=All}");
app.MapControllerRoute(name: "Cities", pattern: "{controller=Cities}/{action=All}");
app.MapControllerRoute(name: "WeatherData", pattern: "{controller=WeatherDta}/{action=All}");

app.Run();
